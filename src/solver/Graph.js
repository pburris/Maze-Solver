/**
 * Graph Data type
 */

// States
const STATE = {
  unvisited: 1,
  visited: 2,
  visiting: 3,
};

/**
 * Node
 */
class Node {
  constructor(value) {
    this.value = value;
    this.visitState = STATE.unvisited;
    this.adjacent = {}; // node: weight
  }
}

/**
 * Graph
 */
export default class Graph {
  constructor() {
    this.nodes = {};
  }

  addNode(val) {
    const n = new Node(val);
    this.nodes[val] = n;
    return n;
  }

  addEdge(src, dest, weight = 0) {
    if (!Object.keys(this.nodes).includes(src)) {
      this.addNode(src);
    }
    if (!Object.keys(this.nodes).includes(dest)) {
      this.addNode(dest);
    }
    this.nodes[src].adjacent[this.nodes[dest]] = weight;
  }
}
