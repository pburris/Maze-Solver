/**
 * Maze Solver
 *
 * 1. Load the image -- Must have 1x1 px black walls and 1x1 px white hallways
 * 2. Turn the image into an array of arrays to loop through
 * 3. Turn the array of arrays into a graph to traverse through
 *    - Find the first white square                                        NODE
 *    - If a square has 2 adjacent white on non-parallel sides             NODE
 *    - If a square has white on 3 sides                                   NODE
 *    - Find the last white square                                         NODE
 *    - If a square has black on 2 parallel sides and has a previous node  SKIP
 *
 * 4. Traverse the graph, attempting to connect the first node with the last
 * 5. Return the new graph with only the nodes in the solution
 * 5. Use the coordinates in the new graph to re-color the image with the solution
 */
import Graph from './Graph';

// Solver
export default class MazeSolver {
  constructor(path) {
    this.imagePath = path;
    this.image = null;
    this.imageGraph = new Graph();
    this.solution = null;
  }
}

// Graph From Maze
const graphFromMaze = (data, w, h) => {
  const g = new Graph();
  const visited = [];
  for (let i = 0; i < data.length; i += 4) {
    const px = i / 4;
    const row = Math.floor(px / h);
    const col = px % w;
    const tag = `[${row}, ${col}]`;
    visited.push(tag);

    const color = `${data[i]} ${data[i + 1]} ${data[i + 2]}`;
    if (color === '0 0 0') {
      // pixel is black aka edge
    } else if (color === '255 255 255') {
      // pixel is white aka hallway

      // Check if left square is black
      if (
        i < 3 ||
        (i > 3 && `${data[i - 2]} ${data[i - 3]} ${data[i - 4]}` === '0 0 0')
      ) {
        g.addNode(tag);
      }

      // Check if the right square is black

      // Check the next square in the row to see if it is white aswell
    }
  }
  return g;
};
