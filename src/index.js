/**
 * Maze Solver Entry
 */
import { loadImage, newCanvas } from './canvas';

const run = async () => {
  // Load image and create canvas with that image
  const img = await loadImage('/public/tiny.png');
  const c = newCanvas({ width: img.width, height: img.height, id: 'maze' });
  const ctx = c.getContext('2d');
  ctx.drawImage(img, 0, 0);
  document.body.appendChild(c);
};

run();
