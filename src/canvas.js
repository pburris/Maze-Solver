/**
 * Canvas -- Dealing with the canvas
 */

/**
 * New Canvas
 * Create a canvas element
 */
export const newCanvas = ({ id, width, height }) => {
  const c = document.createElement('canvas');
  c.setAttribute('id', id);
  c.setAttribute('width', width);
  c.setAttribute('height', height);
  return c;
};

/**
 * Load Image
 */
export const loadImage = path =>
  new Promise((resolve, reject) => {
    const img = new Image();
    img.src = path;
    img.addEventListener('load', () => resolve(img));
    img.addEventListener('error', err => reject(err));
  });
